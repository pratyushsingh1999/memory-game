window.addEventListener('load' , () => {
    const easyButton = document.getElementById('easy');
    const mediumButton = document.getElementById('medium');
    const hardButton = document.getElementById('hard');
    const list = document.getElementById('list');
    const loader = document.getElementById('loader');

    const header = document.createElement('tr');
    const headerRank = document.createElement('th');
    const headerName = document.createElement('th');
    const headerScore = document.createElement('th');
    headerRank.innerText = 'Rank';
    headerName.innerText = 'Player Name';
    headerScore.innerText = 'Best Score';
    header.append(headerRank);
    header.append(headerName);
    header.append(headerScore);
    easyButton.classList.add('active');
    list.style.setProperty('display' , 'none');
    fetch('https://backend-memory-game.herokuapp.com/api/getAllScores/easy')
        .then((data) => {
            return data.json()
        })
        .then((data) => {
            list.style.setProperty('display' , 'table');
            loader.style.setProperty('display', 'none');
            list.append(header);
            data.forEach((details , index) => {
                const newScore = document.createElement('tr');
                const rank = document.createElement('td');
                const name = document.createElement('td');
                const score = document.createElement('td');
                rank.innerText = index + 1;
                name.innerText = details.playerName;
                score.innerText = details.bestScore;
                newScore.append(rank);
                newScore.append(name);
                newScore.append(score);
                list.append(newScore);
            })
        })

    easyButton.addEventListener('click' , () => {
        list.style.setProperty('display','none');
        loader.style.setProperty('display', 'block');
        easyButton.classList.add('active');
        mediumButton.classList.remove('active');
        hardButton.classList.remove('active');
        fetch('https://backend-memory-game.herokuapp.com/api/getAllScores/easy')
            .then((data) => {
                return data.json()
            })
            .then((data) => {
                list.style.setProperty('display','table');
                loader.style.setProperty('display', 'none');
                list.innerHTML = "";
                list.append(header);
                data.forEach((details , index) => {
                    const newScore = document.createElement('tr');
                    const rank = document.createElement('td');
                    const name = document.createElement('td');
                    const score = document.createElement('td');
                    rank.innerText = index + 1;
                    name.innerText = details.playerName;
                    score.innerText = details.bestScore;
                    newScore.append(rank);
                    newScore.append(name);
                    newScore.append(score);
                    list.append(newScore);
                })
            })
    })
    mediumButton.addEventListener('click' , () => {
        list.style.setProperty('display','none');
        loader.style.setProperty('display', 'block');
        easyButton.classList.remove('active');
        mediumButton.classList.add('active');
        hardButton.classList.remove('active');
        fetch('https://backend-memory-game.herokuapp.com/api/getAllScores/medium')
            .then((data) => {
                return data.json()
            })
            .then((data) => {
                list.style.setProperty('display','table');
                loader.style.setProperty('display', 'none');
                list.innerHTML = "";
                list.append(header);
                data.forEach((details , index) => {
                    const newScore = document.createElement('tr');
                    const rank = document.createElement('td');
                    const name = document.createElement('td');
                    const score = document.createElement('td');
                    rank.innerText = index + 1;
                    name.innerText = details.playerName;
                    score.innerText = details.bestScore;
                    newScore.append(rank);
                    newScore.append(name);
                    newScore.append(score);
                    list.append(newScore);
                })
            })
    })
    hardButton.addEventListener('click' , () => {
        list.style.setProperty('display','none');
        loader.style.setProperty('display', 'block');
        easyButton.classList.remove('active');
        mediumButton.classList.remove('active');
        hardButton.classList.add('active');
        fetch('https://backend-memory-game.herokuapp.com/api/getAllScores/hard')
            .then((data) => {
                return data.json()
            })
            .then((data) => {
                list.style.setProperty('display','table');
                loader.style.setProperty('display', 'none');
                list.innerHTML = "";
                list.append(header);
                data.forEach((details , index) => {
                    const newScore = document.createElement('tr');
                    const rank = document.createElement('td');
                    const name = document.createElement('td');
                    const score = document.createElement('td');
                    rank.innerText = index + 1;
                    name.innerText = details.playerName;
                    score.innerText = details.bestScore;
                    newScore.append(rank);
                    newScore.append(name);
                    newScore.append(score);
                    list.append(newScore);
                })
            })
    })
})
