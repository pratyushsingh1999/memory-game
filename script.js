const gameContainer = document.getElementById("game");
const playerName = document.getElementById("player-name");
document.getElementById('error').innerText = 'Name is required to start the game';
document.getElementById('error').style.setProperty('display' , 'block');
document.getElementById('start-game-button').style.setProperty('pointer-events','none');
document.getElementById('start-game-button').style.setProperty('opacity','0.5');
let playerNameDetails;
if (!(getPlayerName() == null)) {
    playerName.value = getPlayerName();
    document.getElementById('start-game-button').style.setProperty('pointer-events','auto');
    document.getElementById('start-game-button').style.setProperty('opacity','1');
    document.getElementById('error').style.setProperty('display' , 'none');
}
playerName.addEventListener('input' , (event) => {
    if(event.target.value.length<1) {
        document.getElementById('start-game-button').style.setProperty('pointer-events','none');
        document.getElementById('start-game-button').style.setProperty('opacity','0.5');
        document.getElementById('error').innerText = 'Name is required to start the game';
        document.getElementById('error').style.setProperty('display' , 'block');
    }else {
        playerNameDetails = event.target.value;
        document.getElementById('start-game-button').style.setProperty('pointer-events','auto');
        document.getElementById('start-game-button').style.setProperty('opacity','1');
        document.getElementById('error').style.setProperty('display' , 'none');
        setNameLocally(playerNameDetails);
    };
})
getRandomColor = () => {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
const allRandomGifs = [
    '1.gif',
    '2.gif',
    '3.gif',
    '4.gif',
    '5.gif',
    '6.gif',
    '7.gif',
    '8.gif',
    '9.gif',
    '10.gif',
    '11.gif',
    '12.gif',
]
let gifsUsed = []
getRandomGif = () => {
    const basePath = './gifs/'
    let randomNumber = Math.floor(Math.random()*11);
    while (gifsUsed.includes(randomNumber)) {
        randomNumber = Math.floor(Math.random()*11);
    }
    gifsUsed.push(randomNumber);
    const completePath = basePath + allRandomGifs[randomNumber];
    return completePath;
}
showHighestScore = () => {
    const highestEasy = localStorage.getItem('6') || 'not played yet';
    const highestMedium = localStorage.getItem('8') || 'not played yet';
    const highestHard = localStorage.getItem('12') || 'not played yet';
    document.getElementById('score-table').style.setProperty('display', 'table');
    document.getElementById('easy-mode').innerText = highestEasy;
    document.getElementById('medium-mode').innerText = highestMedium;
    document.getElementById('hard-mode').innerText = highestHard;
}
function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}
function createDivsForColors(colorArray) {
    for (let color of colorArray) {
        // create a new div
        const newDiv = document.createElement("div");

        // give it a class attribute for the value we are looping over
        newDiv.classList.add(color);

        // call a function handleCardClick when a div is clicked on
        newDiv.addEventListener("click", handleColoursCardClick);

        // append the div to the element with an id of game
        gameContainer.append(newDiv);
    }
}
function createDivsForGifs(gifsArray) {
    for (let src of gifsArray) {
        // create a new div
        const newImg = document.createElement("img");

        // give it a class attribute for the value we are looping over
        newImg.classList.add(src);
        newImg.src = './gifs/ffffff.png';
        // call a function handleCardClick when a div is clicked on
        newImg.addEventListener("click", handleGifsCardClick);

        // append the div to the element with an id of game
        gameContainer.append(newImg);
    }
}

let previousClick;
let currentClick;
let count = 0;
let activeElements;
let gameMode;

function handleColoursCardClick(event) {
    console.log("you clicked", event.target.classList.value);
    currentClick = event;
    count++;
    if (previousClick == undefined) {
        previousClick = currentClick;
        previousClick.target.style.backgroundColor = currentClick.target.classList.value;
        previousClick.target.removeEventListener('click', handleColoursCardClick, false);
        return;
    }
    const currentColor = currentClick.target.classList.value;
    const previousColor = previousClick.target.classList.value;

    if (currentColor === previousColor) {
        previousClick.target.style.backgroundColor = previousColor;
        currentClick.target.style.backgroundColor = currentColor;
        activeElements = activeElements - 2;
        currentClick.target.removeEventListener('click', handleColoursCardClick, false);
        previousClick = undefined;
    } else {
        currentClick.target.style.backgroundColor = currentColor;
        gameContainer.style.pointerEvents = 'none';
        setTimeout(() => {
            currentClick.target.style.backgroundColor = 'white';
            previousClick.target.style.backgroundColor = 'white';
            previousClick.target.addEventListener("click", handleColoursCardClick);
            previousClick = undefined;
            gameContainer.style.pointerEvents = 'auto';
        }, 1000)
    }
    setTimeout(() => {
        if (activeElements === 0) {
            console.log(gameMode);
            let lowestScore = localStorage.getItem(gameMode);
            if (lowestScore == null) {
                localStorage.setItem(gameMode, count);
                lowestScore = count;
            }
            if (count < lowestScore) {
                localStorage.setItem(gameMode, count);
                lowestScore = count;
                congratulateUser(count);
            }

            document.getElementById('score').innerText = "Game Over, your score is: " + count;
            showHighestScore();
            document.getElementById('restart-button').style.setProperty('display' , 'none');
            document.getElementById('playAgain-button').style.setProperty('display','inline-block');
            document.getElementById('open-global-score').style.setProperty('display','block');
            gameContainer.innerHTML = "";
            addGlobalScore(count).then(() => {
                console.log("Score Added");
            })
        }
    },500)
}
function handleGifsCardClick(event) {
    console.log("you clicked", event.target.classList.value);
    currentClick = event;
    if (previousClick == undefined) {
        previousClick = currentClick;
        previousClick.target.src = currentClick.target.classList.value;
        previousClick.target.removeEventListener('click', handleGifsCardClick, false);
        count++;
        return;
    }
    const currentSrc = currentClick.target.classList.value;
    const previousSrc = previousClick.target.classList.value;

    if (currentSrc === previousSrc) {
        previousClick.target.src = previousSrc;
        currentClick.target.src = currentSrc;
        activeElements = activeElements - 2;
        currentClick.target.removeEventListener('click', handleGifsCardClick, false);
        previousClick = undefined;
        count++;
    } else {
        currentClick.target.src = currentSrc;
        gameContainer.style.pointerEvents = 'none';
        setTimeout(() => {
            currentClick.target.src = './gifs/ffffff.png';
            previousClick.target.src = './gifs/ffffff.png';
            previousClick.target.addEventListener("click", handleGifsCardClick);
            previousClick = undefined;
            gameContainer.style.pointerEvents = 'auto';
        }, 1000)
        count++;
    }
    setTimeout(() => {
        if (activeElements === 0) {
            console.log(gameMode);
            let lowestScore = localStorage.getItem(gameMode);
            if (lowestScore == null) {
                localStorage.setItem(gameMode, count);
                lowestScore = count;
            }
            if (count < lowestScore) {
                localStorage.setItem(gameMode, count);
                lowestScore = count;
                congratulateUser(count);
            }

            document.getElementById('score').innerText = "Game Over, your score is: " + count;
            showHighestScore();
            document.getElementById('restart-button').style.setProperty('display' , 'none');
            document.getElementById('playAgain-button').style.setProperty('display','inline-block');
            document.getElementById('open-global-score').style.setProperty('display','block');
            gameContainer.innerHTML = "";
            addGlobalScore(count).then(() => {
                console.log("Score Added");
            })
        }
    },500)
}
// when the DOM loads
restartGame = () => {
    gameContainer.innerHTML = '';
    count = 0;
    gifsUsed = [];
    document.getElementById('score').innerText = ""
    document.getElementById('game-container').style.display = 'none';
    document.getElementById('start-game-container').style.display = 'flex';
    document.getElementById('confirmation').style.setProperty('display', 'none');
    document.getElementById('restart-button').style.setProperty('display', 'inline-block');
    document.getElementById('score-table').style.setProperty('display', 'none');
    document.getElementById('playAgain-button').style.setProperty('display','none');
}
startGame = () => {
    document.getElementById('game-container').style.display = 'block';
    document.getElementById('start-game-container').style.display = 'none';
    const numberOfCards = parseInt(document.getElementById('game-difficulty').value);
    gameMode = numberOfCards;
    let gameType = document.getElementById('load-type').value;
    activeElements = numberOfCards;
    console.log(numberOfCards , gameType);
    switch (gameType) {
        case 'colour':
            startColorGame(numberOfCards);
            break;
        case 'gif':
            startGifGame(numberOfCards);
            break;
    }
}

startColorGame = (numberOfCards) => {
    let coloursArray = [];
    for (let index=0;index<numberOfCards/2;index++) {
        const color = getRandomColor();
        coloursArray.push(color);
        coloursArray.push(color);
    }
    const shuffledColoursArray = shuffle(coloursArray);
    console.log(shuffledColoursArray.length);
    createDivsForColors(shuffledColoursArray);
}
startGifGame = (numberOfCards) => {
    let gifsArray = [];
    for (let index=0;index<numberOfCards/2;index++) {
        const gif = getRandomGif();
        gifsArray.push(gif);
        gifsArray.push(gif);
    }
    const shuffledGifsArray = shuffle(gifsArray);
    console.log(shuffledGifsArray);
    createDivsForGifs(shuffledGifsArray);
}
function openConfirmation() {
    document.getElementById('confirmation').style.setProperty('display', 'block');
    document.getElementById('restart-button').style.setProperty('display', 'none');
}
function dontRestart() {
    document.getElementById('confirmation').style.setProperty('display', 'none');
    document.getElementById('restart-button').style.setProperty('display', 'inline-block');
}
function addGlobalScore(count) {
    const name = getPlayerName() || playerNameDetails;
    let mode;
    const score = count;
    if (gameMode == 6) {
        mode = 'easy';
    }
    if (gameMode == 8) {
        mode = 'medium';
    }
    if (gameMode == 12) {
        mode = 'hard';
    }
    const tempBody = {
        playerName: name,
        level: mode,
        bestScore: score
    }
    return fetch('https://backend-memory-game.herokuapp.com/api/addScore' , {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(tempBody)
    })
}
function setNameLocally(name) {
    localStorage.setItem('playerName' , name);
}
function getPlayerName() {
    return localStorage.getItem('playerName');
}
function congratulateUser (count) {
    console.log("Congratulate user");
    document.getElementById('congratulate-user').style.setProperty('display' , 'flex');
    const button = document.getElementById('send-email-button');
    const input = document.getElementById('congratulate-email');
    const error = document.getElementById('congratulate-email-error');
    button.classList.add('disable');
    input.addEventListener('input' , (event) => {
        if (ValidateEmail(event.target.value)) {
            error.style.setProperty('display','none');
            button.classList.remove('disable');
        }else {
            error.style.setProperty('display','block');
            button.classList.add('disable');
        }
    })
    button.addEventListener('click' , () => {
        button.style.setProperty('display','none');
        document.getElementById('send-email-loader-id').style.setProperty('display','block');
        sendEmail(input.value , count).then(() => {
            document.getElementById('send-email-loader-id').style.setProperty('display','none');
            document.getElementById('congratulate-user').innerHTML = '<p>Mail Sent</p>';
            setTimeout(() => {
                document.getElementById('congratulate-user').style.setProperty('display', 'none');
                document.getElementById('congratulate-user').innerHTML = `<p>Congrats this is you highest score</p>
                                                                                   <p>Enter email so that we can send you the certificate</p>
                                                                                   <input type="email" placeholder="Enter email*" id="congratulate-email">
                                                                                   <p class="congratulate-error" id="congratulate-email-error">Email is required*</p>
                                                                                   <button type="button" id="send-email-button">Send Certificate</button>
                                                                                   <div class="loader send-email-loader" id="send-email-loader-id">
                                                                                   </div>`
            },2000)
        });
    })
}
function sendEmail(email,count) {
    const tempBody = {
        email,
        count
    }
    return fetch('https://backend-memory-game.herokuapp.com/api/sendMail/highestScore' , {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(tempBody)
    })
}
function ValidateEmail(mail) {
    return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail);
}
